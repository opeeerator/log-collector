from django.urls import path

from . import views

urlpatterns = [
    path('all_logs/', views.all_logs),
    path('create_log/', views.create_log),
    path('create_schema/', views.create_schema),
]