from django.db import models
from django.contrib.postgres.fields import JSONField

class Log_record(models.Model):
    """This class represents the loglist model."""
    message = JSONField(db_index=True)
    date_time_created = models.DateTimeField(default=models.fields.datetime.datetime.now)
    client_ip = models.GenericIPAddressField(null=True)
    client_port = models.TextField(null=True, default="Port not found!")

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{} | {} | {}".format(self.date_time_created, self.client_ip, self.client_port)

class Log_schema(models.Model):
    """This class represents the loglist model."""
    message = JSONField(db_index=True)
    date_time_created = models.DateTimeField(default=models.fields.datetime.datetime.now)
    client_ip = models.GenericIPAddressField(null=True)
    client_port = models.TextField(null=True, default="Port not found!")

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{} | {} | {}".format(self.date_time_created, self.client_ip, self.client_port)