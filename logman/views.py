from django.shortcuts import render
from .models import Log_record, Log_schema
from django.core import serializers
from django.http import HttpResponse, JsonResponse, HttpRequest, request
from django.views.decorators.csrf import csrf_exempt
import json
from jsonschema.validators import validate

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_port(request):
    if 'SERVER_PORT' in request.META:
        return request.META['SERVER_PORT']
    else:
        return null

@csrf_exempt
def all_logs(request):
    query_display = request.POST.get('display')
    query_count = request.POST.get('count')
    query_namespace = request.POST.get('namespace')
    query_field_1 = request.POST.get('field_1')
    query_field_1_value = request.POST.get('field_1_value')
    # Quering based-on creation date #################
    if query_count == None:
        query_count = 1
    if query_display == "ascending":
        all_logs = Log_record.objects.all().order_by('date_time_created')[:int(query_count)]
        all_logs_serialized = serializers.serialize('json', all_logs)
        all_logs_json = json.loads(all_logs_serialized)
        data = json.dumps(all_logs_json)
        return HttpResponse(data)
    if query_display == "descending":
        all_logs = Log_record.objects.all().order_by('-date_time_created')[:int(query_count)]
        all_logs_serialized = serializers.serialize('json', all_logs)
        all_logs_json = json.loads(all_logs_serialized)
        data = json.dumps(all_logs_json)
        return HttpResponse(data)
    # Query based-on namespace and field value
    if query_field_1 is not None and query_field_1_value is not None :
        if query_field_1_value.isnumeric():
            all_logs = Log_record.objects.filter(message__payload__contains={str(query_field_1): int(query_field_1_value)})
        else:
            all_logs = Log_record.objects.filter(message__payload__contains={str(query_field_1): query_field_1_value})    
        all_logs_serialized = serializers.serialize('json', all_logs)
        all_logs_json = json.loads(all_logs_serialized)
        data = json.dumps(all_logs_json)
        return HttpResponse(data)
    if query_namespace is not None and query_field_1 is None:
        all_logs = Log_record.objects.filter(message__contains={'namespace':query_namespace})
        all_logs_serialized = serializers.serialize('json', all_logs)
        all_logs_json = json.loads(all_logs_serialized)
        data = json.dumps(all_logs_json)
        return HttpResponse(data)
    else:
        all_logs = Log_record.objects.all()[:int(query_count)]
        all_logs_serialized = serializers.serialize('json', all_logs)
        all_logs_json = json.loads(all_logs_serialized)
        data = json.dumps(all_logs_json)
        return HttpResponse(data)


@csrf_exempt
def create_schema(request):

    try:
        message = json.loads(request.body)
        client_ip = get_client_ip(request)
        client_port = get_port(request)

        schema_instance = Log_schema()
        schema_instance.message = message
        schema_instance.client_ip = client_ip
        schema_instance.client_port = client_port
        schema_instance.save()
        
        return HttpResponse("200")
    except:
        return HttpResponse("There was a problem in storing client's Log_schema!")

@csrf_exempt
def create_log(request):
    try:
        json_data = json.loads(request.body)
        namespace = json_data['namespace']
        schema = Log_schema.objects.filter(message__contains={'namespace':namespace})
    except:
        return HttpResponse("Schema not found!")
    """
    try:
        validate(instance=json_data, schema=schema)
    except:
        return HttpResponse("Not valid data!")
    """
    try:
        message = json_data
        client_ip = get_client_ip(request)
        client_port = get_port(request)

        log_instance = Log_record()
        log_instance.message = message
        log_instance.client_ip = client_ip
        log_instance.client_port = client_port
        log_instance.save()
        
        return HttpResponse("200")
    except:
        return HttpResponse("There was a problem in storing client's log!")