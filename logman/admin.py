from django.contrib import admin
from .models import Log_record, Log_schema

admin.site.register(Log_record)
admin.site.register(Log_schema)